module ApplicationHelper
  def page_header(page_title)
    html = content_tag :div, page_title, class: 'h3 mb-4'
    content_for :title, html
  end

  def nice_short_list(data, field, limit = 5)
    data.limit(limit)
        .map { |item| item[field] }
        .join(', ')
  end
end
