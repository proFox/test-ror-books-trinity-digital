class FillTheDatabase < ActiveRecord::Migration[5.2]
  def change
    names = [
      'Wilburn Braun',
      'Sigrid Earles',
      'Ursula Neisler',
      'Willodean Janicki',
      'Nga Guilbault',
      'Kathy Mager',
      'Cassi Eller',
      'Mui Constant',
      'Deeann Ritacco',
      'Les Kaufmann',
      'Janise Selders',
      'Joycelyn Shreve',
      'Virgina Sobel'
    ]

    titles = [
      'Priest With Honor',
      'Savior Of Stone',
      'Guardians Of The World',
      'Cats Of The West',
      'Children And Officers',
      'Dogs And Humans',
      'Dishonor Of The Eternal',
      'Defeat Of The Frontline',
      'Traces In The Immortals',
      'Death To A Storm',
      'Vulture Without Glory',
      'Foreigner Of Heaven',
      'Thieves Of The Prison',
      'Descendants Without A Home',
      'Officers And Slaves',
      'Heirs And Butchers',
      'Shield Of Joy',
      'Ascension Without A Home',
      'Reach Of History',
      'Calling The World',
      'Nymph With Strength',
      'Serpent With Honor',
      'Gangsters Of Joy',
      'Foes Of The Gods',
      'Warriors And Defenders',
      'Trees And Giants',
      'Climax Of The Stockades',
      'Intention Of The Land',
      'Welcome To The Ashes',
      'Scared At The Ocean'
    ]

    names.each do |name|
      Author.find_or_create_by(name: name)
    end

    authors_ids = Author.pluck(:id)

    titles.each do |title|
      book = Book.find_or_create_by(title: title)
      book.authors << Author.find(authors_ids.sample)
    end
  end
end
