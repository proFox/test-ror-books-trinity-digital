require 'rails_helper'

RSpec.describe Author, type: :model do
  context 'creature' do
    it 'with a normal name' do
      Author.create(name: 'God name')
      expect(Author.count).to be >= 1
    end

    it 'with a short name' do
      author = Author.new(name: 'TD')
      author.valid?
      expect(author.errors[:name].size).to eq(1)
    end

    it 'with a long name' do
      author = Author.new(name: 'TD' * 200)
      author.valid?
      expect(author.errors[:name].size).to eq(1)
    end

    it 'with the addition of two new books' do
      author = Author.create(name: 'God name')
      book1 = Book.create(title: 'Book 1')
      book2 = Book.create(title: 'Book 2')

      author.books << book1
      author.books << book2

      expect(author.books.count).to eq(2)
    end
  end
end
